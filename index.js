const express = require('express')
const axios = require('axios')
const cors = require('cors')
const app = express()

app.use(cors())
app.use(express.json());

app.post('/', async (req, res) => {
    try {
        const data = await axios.post('https://api.jdoodle.com/v1/execute', {
            script: req.body.value,
            language: "nodejs",
            versionIndex: "4",
            stdin : '10\n2\n324\ngdfsg',
            clientId: "2bc728603fbca3955ce6f09d12e5c0ee",
            clientSecret: "f4d55a59995a19f02cfe7f6f25defbc91c408c7a3a699881d250555eb01d25e2"
        })
        res.send(data.data)
    } catch (error) {
        console.log(error)
    }
})

app.listen(4000, () => {
    console.log("running")
})